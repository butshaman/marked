/**
 * marked
 * fork: [f711b40](https://github.com/egoist/marked3)
 * with: [55ab987](https://github.com/chjj/marked/blob/master/lib/marked.js)
 * support plugins: https://github.com/chjj/marked/pull/846
 */

import * as utils from './utils'
import defaultOptions from './defaults'
import Parser from './parser'
import Lexer from './lexer'
import Renderer, { TextRenderer } from './renderer'
import InlineLexer from './inline-lexer'

function marked(src, opt) {
  // throw error in case of non string input
  if (typeof src === 'undefined' || src === null)
    throw new Error('marked(): input parameter is undefined or null')
  if (typeof src !== 'string')
    throw new Error(
      'marked(): input parameter is of type ' +
        Object.prototype.toString.call(src) +
        ', string expected'
    )

  try {
    if (opt) opt = utils.merge({}, defaultOptions, opt)
    return Parser.parse(Lexer.lex(src, opt), opt)
  } catch (err) {
    if ((opt || defaultOptions).silent) {
      return (
        '<p>An error occurred:</p><pre>' +
        utils.escape(String(err.message), true) +
        '</pre>'
      )
    }
    throw err
  }
}

marked.Renderer = Renderer
marked.TextRenderer = TextRenderer
marked.Parser = Parser
marked.Lexer = Lexer
marked.InlineLexer = InlineLexer
marked.defaults = defaultOptions
marked.utils = utils

export default marked
