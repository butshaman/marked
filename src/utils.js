function merge(obj) {
  let i = 1
  let target
  let key

  for (; i < arguments.length; i++) {
    target = arguments[i]
    for (key in target) {
      if (Object.prototype.hasOwnProperty.call(target, key)) {
        obj[key] = target[key]
      }
    }
  }

  return obj
}

function noop() {}
noop.exec = noop

function escape(html, encode) {
  return html
    .replace(encode ? /&/g : /&(?!#?\w+;)/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&#39;')
}

function unescape(html) {
  // explicitly match decimal, hex, and named HTML entities
  return html.replace(/&(#(?:\d+)|(?:#x[0-9A-Fa-f]+)|(?:\w+));?/gi, (_, n) => {
    n = n.toLowerCase()
    if (n === 'colon') return ':'
    if (n.charAt(0) === '#') {
      return n.charAt(1) === 'x'
        ? String.fromCharCode(parseInt(n.substring(2), 16))
        : String.fromCharCode(Number(n.substring(1)))
    }
    return ''
  })
}

function edit(regex, opt) {
  regex = regex.source
  opt = opt || ''
  return {
    replace(name, val) {
      val = val.source || val
      val = val.replace(/(^|[^\[])\^/g, '$1')
      regex = regex.replace(name, val)
      return this
    },
    getRegex() {
      return new RegExp(regex, opt)
    }
  }
}

const originIndependentUrl = /^$|^[a-z][a-z0-9+.-]*:|^[?#]/i
const noLastSlashUrl = /^[^:]+:\/*[^/]*$/
const baseUrls = {}

function resolveUrl(base, href) {
  if (originIndependentUrl.test(href)) {
    return href
  }

  if (!baseUrls[' ' + base]) {
    // we can ignore everything in base after the last slash of its path component,
    // but we might need to add _that_
    // https://tools.ietf.org/html/rfc3986#section-3
    if (noLastSlashUrl.test(base)) {
      baseUrls[' ' + base] = base + '/'
    } else {
      baseUrls[' ' + base] = base.replace(/[^/]*$/, '')
    }
  }

  base = baseUrls[' ' + base]

  if (href.slice(0, 2) === '//') {
    return base.replace(/:[\s\S]*/, ':') + href
  } else if (href.charAt(0) === '/') {
    return base.replace(/(:\/*[^/]*)[\s\S]*/, '$1') + href
  } else {
    return base + href
  }
}

export { merge, noop, escape, unescape, edit, resolveUrl }
