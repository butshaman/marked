import Renderer from './renderer'

export default {
  gfm: true,
  tables: true,
  taskLists: true,
  breaks: false,
  extra: false,
  pedantic: false,
  sanitize: false,
  sanitizer: null,
  mangle: true,
  smartLists: false,
  silent: false,
  highlight: null,
  langPrefix: 'lang-',
  smartypants: false,
  xhtml: false,
  headerPrefix: '',
  renderer: new Renderer(),
  plugins: [],
  disabledRules: [],
  // [Expose an option for rendering image/link URLs relative to a base](https://github.com/chjj/marked/pull/943)
  baseUrl: null
}
