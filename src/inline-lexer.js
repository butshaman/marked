import defaultOptions from './defaults'
import Renderer from './renderer'
import inlineRules from './inline-rules'
import { escape, noop } from './utils'

class InlineLexer {
  constructor(
    links,
    options = defaultOptions,
    footnotes = {},
    usedFootnotes = []
  ) {
    this.options = options
    this.footnotes = footnotes
    this.usedFootnotes = usedFootnotes
    this.footnoteCounter = 0
    this.links = links
    this.renderer = this.options.renderer || new Renderer()
    this.renderer.options = this.options

    if (!this.links) {
      throw new Error('Tokens array requires a `links` property.')
    }

    if (this.options.extra) {
      this.rules = inlineRules.extra
    } else if (this.options.gfm) {
      if (this.options.breaks) {
        this.rules = inlineRules.breaks
      } else {
        this.rules = inlineRules.gfm
      }
    } else if (this.options.pedantic) {
      this.rules = inlineRules.pedantic
    } else {
      this.rules = inlineRules.normal
    }

    // plugins rules
    let text = this.rules.text.toString()
    this.options.plugins.forEach((plugin) => {
      if (plugin.inline && plugin.inline.text) {
        text = text.replace(']|', plugin.inline.text + ']|')
      }
    })
    this.rules.text = new RegExp(text.substring(1, text.length - 1))

    if (Array.isArray(this.options.disabledRules)) {
      this.options.disabledRules.forEach((ruleName) => {
        if (this.rules[ruleName]) {
          this.rules[ruleName] = noop
        }
      })
    }
  }

  static output(src, links, options, footnotes, usedFootnotes) {
    return new InlineLexer(links, options, footnotes, usedFootnotes).output(src)
  }

  output(src) {
    const plugins = this.options.plugins
    let out = ''
    let link
    let text
    let href
    let cap
    let plugin

    while (src) {
      // escape
      if ((cap = this.rules.escape.exec(src))) {
        src = src.substring(cap[0].length)
        out += cap[1]
        continue
      }

      // autolink
      if ((cap = this.rules.autolink.exec(src))) {
        src = src.substring(cap[0].length)
        if (cap[2] === '@') {
          text = escape(this.mangle(cap[1]))
          href = 'mailto:' + text
        } else {
          text = escape(cap[1])
          href = text
        }
        out += this.renderer.link(href, null, text)
        continue
      }

      // url (gfm)
      if (!this.inLink && (cap = this.rules.url.exec(src))) {
        cap[0] = this.rules._backpedal.exec(cap[0])[0]
        src = src.substring(cap[0].length)
        if (cap[2] === '@') {
          text = escape(cap[0])
          href = 'mailto:' + text
        } else {
          text = escape(cap[0])
          if (cap[1] === 'www.') {
            href = 'http://' + text
          } else {
            href = text
          }
        }
        out += this.renderer.link(href, null, text)
        continue
      }

      // tag
      if ((cap = this.rules.tag.exec(src))) {
        if (!this.inLink && /^<a /i.test(cap[0])) {
          this.inLink = true
        } else if (this.inLink && /^<\/a>/i.test(cap[0])) {
          this.inLink = false
        }
        src = src.substring(cap[0].length)
        out += this.options.sanitize
          ? this.options.sanitizer
            ? this.options.sanitizer(cap[0])
            : escape(cap[0])
          : cap[0]
        continue
      }

      // footnotes (extra)
      if ((cap = this.rules.footnote.exec(src))) {
        src = src.substring(cap[0].length)
        let m1 = escape(cap[1].toLowerCase())
        if (this.footnotes[m1]) {
          ++this.footnoteCounter
          this.usedFootnotes.push(m1)
          out += `<sup id="cite-ref-${m1}" class="reference"><a href="#cite-${m1}">[${
            this.footnoteCounter
          }]</a></sup>`
        } else {
          out += this.options.sanitize ? escape(cap[0]) : cap[0]
        }
        continue
      }

      // link
      if ((cap = this.rules.link.exec(src))) {
        src = src.substring(cap[0].length)
        this.inLink = true
        out += this.outputLink(cap, {
          href: cap[2],
          title: cap[3]
        })
        this.inLink = false
        continue
      }

      // reflink, nolink
      if (
        (cap = this.rules.reflink.exec(src)) ||
        (cap = this.rules.nolink.exec(src))
      ) {
        src = src.substring(cap[0].length)
        link = (cap[2] || cap[1]).replace(/\s+/g, ' ')
        link = this.links[link.toLowerCase()]
        if (!link || !link.href) {
          out += cap[0].charAt(0)
          src = cap[0].substring(1) + src
          continue
        }
        this.inLink = true
        out += this.outputLink(cap, link)
        this.inLink = false
        continue
      }

      // strong
      if ((cap = this.rules.strong.exec(src))) {
        src = src.substring(cap[0].length)
        out += this.renderer.strong(this.output(cap[2] || cap[1]))
        continue
      }

      // em
      if ((cap = this.rules.em.exec(src))) {
        src = src.substring(cap[0].length)
        out += this.renderer.em(this.output(cap[2] || cap[1]))
        continue
      }

      // code
      if ((cap = this.rules.code.exec(src))) {
        src = src.substring(cap[0].length)
        out += this.renderer.codespan(escape(cap[2].trim(), true))
        continue
      }

      // br
      if ((cap = this.rules.br.exec(src))) {
        src = src.substring(cap[0].length)
        out += this.renderer.br()
        continue
      }

      // del (gfm)
      if ((cap = this.rules.del.exec(src))) {
        src = src.substring(cap[0].length)
        out += this.renderer.del(this.output(cap[1]))
        continue
      }

      // plugins
      let isPluginMatch
      for (let index = 0; index < plugins.length; index++) {
        plugin = plugins[index]
        if (plugin.inline) {
          if ((cap = plugin.inline.rule.exec(src))) {
            src = src.substring(cap[0].length)
            out += plugin.render(cap)
            isPluginMatch = true
            break
          }
        }
      }
      if (isPluginMatch) continue

      // text
      if ((cap = this.rules.text.exec(src))) {
        src = src.substring(cap[0].length)
        out += this.renderer.text(escape(this.smartypants(cap[0])))
        continue
      }

      if (src) {
        throw new Error('Infinite loop on byte: ' + src.charCodeAt(0))
      }
    }

    return out
  }

  outputLink(cap, link) {
    const href = escape(link.href)
    const title = link.title ? escape(link.title) : null

    return cap[0].charAt(0) === '!'
      ? this.renderer.image(href, title, escape(cap[1]))
      : this.renderer.link(href, title, this.output(cap[1]))
  }

  smartypants(text) {
    if (!this.options.smartypants) return text
    return (
      text
        // em-dashes
        .replace(/---/g, '\u2014')
        // en-dashes
        .replace(/--/g, '\u2013')
        // opening singles
        .replace(/(^|[-\u2014/(\[{"\s])'/g, '$1\u2018') // eslint-disable-line
        // closing singles & apostrophes
        .replace(/'/g, '\u2019')
        // opening doubles
        .replace(/(^|[-\u2014/(\[{\u2018\s])"/g, '$1\u201c') // eslint-disable-line
        // closing doubles
        .replace(/"/g, '\u201d')
        // ellipses
        .replace(/\.{3}/g, '\u2026')
    )
  }

  mangle(text) {
    if (!this.options.mangle) return text
    let out = ''
    let i = 0
    let ch

    for (; i < text.length; i++) {
      ch = text.charCodeAt(i)
      if (Math.random() > 0.5) {
        ch = 'x' + ch.toString(16)
      }
      out += '&#' + ch + ';'
    }

    return out
  }
}

InlineLexer.rules = inlineRules

export default InlineLexer
