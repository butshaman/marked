import blockRules from './block-rules'
import defaultOptions from './defaults'
import { escape, noop } from './utils'

class Lexer {
  constructor(options = defaultOptions) {
    this.tokens = []
    this.tokens.links = {}
    this.tokens.footnotes = {}
    this.tokens.ext = {}
    this.options = options

    if (this.options.extra) {
      this.rules = blockRules.extra
    } else if (this.options.gfm) {
      if (this.options.tables) {
        this.rules = blockRules.tables
      } else {
        this.rules = blockRules.gfm
      }
    } else {
      this.rules = blockRules.normal
    }

    if (Array.isArray(this.options.disabledRules)) {
      this.options.disabledRules.forEach((ruleName) => {
        if (this.rules[ruleName]) {
          this.rules[ruleName] = noop
        }
      })
    }
  }

  static lex(src, options) {
    return new Lexer(options).lex(src)
  }

  lex(src) {
    src = src
      .replace(/\r\n|\r/g, '\n')
      .replace(/\t/g, '    ')
      .replace(/\u00a0/g, ' ')
      .replace(/\u2424/g, '\n')

    return this.token(src, true)
  }

  token(src, top) {
    src = src.replace(/^ +$/gm, '')
    const plugins = this.options.plugins

    let next
    let loose
    let cap
    let bull
    let b
    let item
    let space
    let i
    let tag
    let l
    let checked
    let plugin

    while (src) {
      // newline
      if ((cap = this.rules.newline.exec(src))) {
        src = src.substring(cap[0].length)
        if (cap[0].length > 1) {
          this.tokens.push({
            type: 'space'
          })
        }
      }

      // code
      if ((cap = this.rules.code.exec(src))) {
        src = src.substring(cap[0].length)
        cap = cap[0].replace(/^ {4}/gm, '')
        this.tokens.push({
          type: 'code',
          text: this.options.pedantic ? cap : cap.replace(/\n+$/, '')
        })
        continue
      }

      // fences (gfm)
      if ((cap = this.rules.fences.exec(src))) {
        src = src.substring(cap[0].length)
        this.tokens.push({
          type: 'code',
          lang: cap[2],
          text: cap[3] || ''
        })
        continue
      }

      // heading
      if ((cap = this.rules.heading.exec(src))) {
        src = src.substring(cap[0].length)
        this.tokens.push({
          type: 'heading',
          depth: cap[1].length,
          text: cap[2]
        })
        continue
      }

      // table no leading pipe (gfm)
      if (top && (cap = this.rules.nptable.exec(src))) {
        src = src.substring(cap[0].length)

        item = {
          type: 'table',
          header: cap[1] && cap[1].replace(/^ *| *\| *$/g, '').split(/ *\| */),
          align: cap[2].replace(/^ *|\| *$/g, '').split(/ *\| */),
          cells: cap[3].replace(/\n$/, '').split('\n')
        }

        for (i = 0; i < item.align.length; i++) {
          if (/^ *-+: *$/.test(item.align[i])) {
            item.align[i] = 'right'
          } else if (/^ *:-+: *$/.test(item.align[i])) {
            item.align[i] = 'center'
          } else if (/^ *:-+ *$/.test(item.align[i])) {
            item.align[i] = 'left'
          } else {
            item.align[i] = null
          }
        }

        for (i = 0; i < item.cells.length; i++) {
          item.cells[i] = item.cells[i].split(/ *[^`\\]\| */)
        }

        this.tokens.push(item)

        continue
      }

      // hr
      if ((cap = this.rules.hr.exec(src))) {
        src = src.substring(cap[0].length)
        this.tokens.push({
          type: 'hr'
        })
        continue
      }

      // blockquote
      if ((cap = this.rules.blockquote.exec(src))) {
        src = src.substring(cap[0].length)

        this.tokens.push({
          type: 'blockquote_start'
        })

        cap = cap[0].replace(/^ *> ?/gm, '')

        // Pass `top` to keep the current
        // "toplevel" state. This is exactly
        // how markdown.pl works.
        this.token(cap, top)

        this.tokens.push({
          type: 'blockquote_end'
        })

        continue
      }

      // list
      if ((cap = this.rules.list.exec(src))) {
        src = src.substring(cap[0].length)
        bull = cap[2]

        this.tokens.push({
          type: 'list_start',
          ordered: bull.length > 1
        })

        // Get each top-level item.
        cap = cap[0].match(this.rules.item)

        next = false
        l = cap.length
        i = 0

        for (; i < l; i++) {
          item = cap[i]

          // Remove the list item's bullet
          // so it is seen as the next token.
          space = item.length
          item = item.replace(/^ *([*+-]|\d+\.) +/, '')

          if (this.options.gfm && this.options.taskLists) {
            checked = this.rules.checkbox.exec(item)

            if (checked) {
              checked = checked[1] === 'x'
              item = item.replace(this.rules.checkbox, '')
            } else {
              checked = undefined
            }
          }

          // Outdent whatever the
          // list item contains. Hacky.
          if (item.indexOf('\n ') !== -1) {
            space -= item.length
            item = this.options.pedantic
              ? item.replace(/^ {1,4}/gm, '')
              : item.replace(new RegExp('^ {1,' + space + '}', 'gm'), '')
          }

          // Determine whether the next list item belongs here.
          // Backpedal if it does not belong in this list.
          if (this.options.smartLists && i !== l - 1) {
            b = this.rules.bullet.exec(cap[i + 1])[0]
            if (bull !== b && !(bull.length > 1 && b.length > 1)) {
              src = cap.slice(i + 1).join('\n') + src
              i = l - 1
            }
          }

          // Determine whether item is loose or not.
          // Use: /(^|\n)(?! )[^\n]+\n\n(?!\s*$)/
          // for discount behavior.
          loose = next || /\n\n(?!\s*$)/.test(item)
          if (i !== l - 1) {
            next = item.charAt(item.length - 1) === '\n'
            if (!loose) loose = next
          }

          this.tokens.push({
            checked,
            type: loose ? 'loose_item_start' : 'list_item_start'
          })

          // Recurse.
          this.token(item, false)

          this.tokens.push({
            type: 'list_item_end'
          })
        }

        this.tokens.push({
          type: 'list_end'
        })

        continue
      }

      // html
      if ((cap = this.rules.html.exec(src))) {
        src = src.substring(cap[0].length)
        this.tokens.push({
          type: this.options.sanitize ? 'paragraph' : 'html',
          pre:
            !this.options.sanitizer &&
            (cap[1] === 'pre' || cap[1] === 'script' || cap[1] === 'style'),
          text: cap[0]
        })
        continue
      }

      // def
      if (top && (cap = this.rules.def.exec(src))) {
        src = src.substring(cap[0].length)
        if (cap[3]) cap[3] = cap[3].substring(1, cap[3].length - 1)
        tag = cap[1].toLowerCase()
        if (!this.tokens.links[tag]) {
          this.tokens.links[tag] = {
            href: cap[2],
            title: cap[3]
          }
        }
        continue
      }

      // table (gfm)
      if (top && (cap = this.rules.table.exec(src))) {
        src = src.substring(cap[0].length)

        item = {
          type: 'table',
          header: cap[1] && cap[1].replace(/^ *| *\| *$/g, '').split(/ *\| */),
          align: cap[2].replace(/^ *|\| *$/g, '').split(/ *\| */),
          // [Fix: does not display of the table cell if it empty](https://github.com/chjj/marked/pull/697)
          cells: cap[3].replace(/\n$/, '').split('\n')
        }

        for (i = 0; i < item.align.length; i++) {
          if (/^ *-+: *$/.test(item.align[i])) {
            item.align[i] = 'right'
          } else if (/^ *:-+: *$/.test(item.align[i])) {
            item.align[i] = 'center'
          } else if (/^ *:-+ *$/.test(item.align[i])) {
            item.align[i] = 'left'
          } else {
            item.align[i] = null
          }
        }

        for (i = 0; i < item.cells.length; i++) {
          if (!item.cells[i].match(/\|$/)) {
            item.cells[i] = item.cells[i] + '|'
          }
          item.cells[i] = item.cells[i]
            .replace(/^ *\| *| *\| *$/g, '')
            // Fix escaping '|' in table: #595 #831 (marked issues)
            .split(/ *[^`\\]\| */)
        }

        this.tokens.push(item)

        continue
      }

      // footnote (extra)
      if ((cap = this.rules.footnote.exec(src))) {
        src = src.substring(cap[0].length)
        this.tokens.footnotes[escape(cap[1].toLowerCase())] = cap[2]
        continue
      }

      // lheading
      if ((cap = this.rules.lheading.exec(src))) {
        src = src.substring(cap[0].length)
        this.tokens.push({
          type: 'heading',
          depth: cap[2] === '=' ? 1 : 2,
          text: cap[1]
        })
        continue
      }

      // plugins
      let isPluginMatch
      for (let index = 0; index < plugins.length; index++) {
        plugin = plugins[index]
        if (plugin.block) {
          if ((cap = plugin.block.rule.exec(src))) {
            if (cap.index === 0) {
              src = src.substring(cap[0].length)
              const token = plugin.block.tokenize(cap)
              this.tokens.push(token)
              isPluginMatch = true
              break
            }
          }
        }
      }
      if (isPluginMatch) continue

      // top-level paragraph
      if (top && (cap = this.rules.paragraph.exec(src))) {
        src = src.substring(cap[0].length)
        this.tokens.push({
          type: 'paragraph',
          text:
            cap[1].charAt(cap[1].length - 1) === '\n'
              ? cap[1].slice(0, -1)
              : cap[1]
        })
        continue
      }

      // text
      if ((cap = this.rules.text.exec(src))) {
        // Top-level should never reach here.
        src = src.substring(cap[0].length)
        this.tokens.push({
          type: 'text',
          text: cap[0]
        })
        continue
      }

      if (src) {
        throw new Error('Infinite loop on byte: ' + src.charCodeAt(0))
      }
    }

    return this.tokens
  }
}

Lexer.rules = blockRules

export default Lexer
