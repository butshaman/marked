import slugo from 'slugo';

function merge(obj) {
  let i = 1;
  let target;
  let key;
  for (; i < arguments.length; i++) {
    target = arguments[i];
    for (key in target) {
      if (Object.prototype.hasOwnProperty.call(target, key)) {
        obj[key] = target[key];
      }
    }
  }
  return obj;
}
function noop() {}
noop.exec = noop;
function escape(html, encode) {
  return html.replace(encode ? /&/g : /&(?!#?\w+;)/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&#39;');
}
function unescape(html) {
  return html.replace(/&(#(?:\d+)|(?:#x[0-9A-Fa-f]+)|(?:\w+));?/gi, (_, n) => {
    n = n.toLowerCase();
    if (n === 'colon') return ':';
    if (n.charAt(0) === '#') {
      return n.charAt(1) === 'x' ? String.fromCharCode(parseInt(n.substring(2), 16)) : String.fromCharCode(Number(n.substring(1)));
    }
    return '';
  });
}
function edit(regex, opt) {
  regex = regex.source;
  opt = opt || '';
  return {
    replace(name, val) {
      val = val.source || val;
      val = val.replace(/(^|[^\[])\^/g, '$1');
      regex = regex.replace(name, val);
      return this;
    },
    getRegex() {
      return new RegExp(regex, opt);
    }
  };
}
const originIndependentUrl = /^$|^[a-z][a-z0-9+.-]*:|^[?#]/i;
const noLastSlashUrl = /^[^:]+:\/*[^/]*$/;
const baseUrls = {};
function resolveUrl(base, href) {
  if (originIndependentUrl.test(href)) {
    return href;
  }
  if (!baseUrls[' ' + base]) {
    if (noLastSlashUrl.test(base)) {
      baseUrls[' ' + base] = base + '/';
    } else {
      baseUrls[' ' + base] = base.replace(/[^/]*$/, '');
    }
  }
  base = baseUrls[' ' + base];
  if (href.slice(0, 2) === '//') {
    return base.replace(/:[\s\S]*/, ':') + href;
  } else if (href.charAt(0) === '/') {
    return base.replace(/(:\/*[^/]*)[\s\S]*/, '$1') + href;
  } else {
    return base + href;
  }
}


var utils = Object.freeze({
	merge: merge,
	noop: noop,
	escape: escape,
	unescape: unescape,
	edit: edit,
	resolveUrl: resolveUrl
});

class Renderer {
  constructor(options) {
    this.options = options || {};
    this._headings = [];
  }
  code(code, lang, escaped) {
    if (this.options.highlight) {
      const out = this.options.highlight(code, lang);
      if (out !== null && out !== code) {
        escaped = true;
        code = out;
      }
    }
    if (!lang) {
      return `<pre><code>${escaped ? code : escape(code, true)}\n</code></pre>`;
    }
    return `<pre><code class="${this.options.langPrefix}${escape(lang, true)}">${escaped ? code : escape(code, true)}\n</code></pre>\n`;
  }
  blockquote(quote) {
    return `<blockquote>\n${quote}</blockquote>\n`;
  }
  html(html) {
    return html;
  }
  heading(text, level, raw) {
    let slug = slugo(raw);
    const count = this._headings.filter(h => h === raw).length;
    if (count > 0) {
      slug += `-${count}`;
    }
    this._headings.push(raw);
    return `<h${level} id="${this.options.headerPrefix}${slug}">${text}</h${level}>\n`;
  }
  hr() {
    return this.options.xhtml ? '<hr/>\n' : '<hr>\n';
  }
  list(body, ordered, taskList) {
    const type = ordered ? 'ol' : 'ul';
    const classNames = taskList ? ' class="task-list"' : '';
    return `<${type}${classNames}>\n${body}</${type}>\n`;
  }
  listitem(text, checked) {
    if (checked === undefined) {
      return `<li>${text}</li>\n`;
    }
    return '<li class="task-list-item">' + '<input type="checkbox" class="task-list-item-checkbox"' + (checked ? ' checked' : '') + '> ' + text + '</li>\n';
  }
  paragraph(text) {
    return `<p>${text}</p>\n`;
  }
  table(header, body) {
    const thead = header ? `\n<thead>\n${header}</thead>` : '';
    return `<table>${thead}\n<tbody>\n${body}</tbody>\n</table>\n`;
  }
  tablerow(content) {
    return `<tr>\n${content}</tr>\n`;
  }
  tablecell(content, flags) {
    const type = flags.header ? 'th' : 'td';
    const tag = flags.align ? `<${type} style="text-align:${flags.align}">` : `<${type}>`;
    return `${tag + content}</${type}>\n`;
  }
  strong(text) {
    return `<strong>${text}</strong>`;
  }
  em(text) {
    return `<em>${text}</em>`;
  }
  codespan(text) {
    return `<code>${text}</code>`;
  }
  br() {
    return this.options.xhtml ? '<br/>' : '<br>';
  }
  del(text) {
    return `<del>${text}</del>`;
  }
  link(href, title, text) {
    if (this.options.sanitize) {
      let prot;
      try {
        prot = decodeURIComponent(unescape(href)).replace(/[^\w:]/g, '').toLowerCase();
      } catch (err) {
        return text;
      }
      if (
      prot.indexOf('javascript:') === 0 || prot.indexOf('vbscript:') === 0 || prot.indexOf('data:') === 0) {
        return `[${text}](${prot})`;
      }
    }
    if (this.options.baseUrl) {
      href = resolveUrl(this.options.baseUrl, href);
    }
    let out = `<a href="${href}"`;
    if (title) {
      out += ` title="${title}"`;
    }
    const { linksInNewTab } = this.options;
    const isLinksInNewTab = linksInNewTab instanceof Function ? linksInNewTab(href) : !!linksInNewTab;
    if (isLinksInNewTab) {
      out += ` target="_blank"`;
    }
    out += `>${text}</a>`;
    return out;
  }
  image(href, title, text) {
    if (this.options.baseUrl) {
      href = resolveUrl(this.options.baseUrl, href);
    }
    let out = `<img src="${href}" alt="${text}"`;
    if (title) {
      out += ` title="${title}"`;
    }
    out += this.options.xhtml ? '/>' : '>';
    return out;
  }
  text(text) {
    return text;
  }
}
class TextRenderer {
  strong(text) {
    return text;
  }
  em(text) {
    return text;
  }
  codespan(text) {
    return text;
  }
  del(text) {
    return text;
  }
  text(text) {
    return text;
  }
  link(href, title, text) {
    return '' + text;
  }
  image(href, title, text) {
    return '' + text;
  }
  br() {
    return '';
  }
}

var defaultOptions = {
  gfm: true,
  tables: true,
  taskLists: true,
  breaks: false,
  extra: false,
  pedantic: false,
  sanitize: false,
  sanitizer: null,
  mangle: true,
  smartLists: false,
  silent: false,
  highlight: null,
  langPrefix: 'lang-',
  smartypants: false,
  xhtml: false,
  headerPrefix: '',
  renderer: new Renderer(),
  plugins: [],
  disabledRules: [],
  baseUrl: null
};

const inline = {
  escape: /^\\([\\`*{}[\]()#+\-.!_>])/,
  autolink: /^<(scheme:[^\s\x00-\x1f<>]*|email)>/,
  url: noop,
  tag: /^<!--[\s\S]*?-->|^<\/?[a-zA-Z0-9\-]+(?:"[^"]*"|'[^']*'|\s[^<'">\/]*)*?\/?>/,
  link: /^!?\[(inside)\]\(href\)/,
  reflink: /^!?\[(inside)\]\s*\[([^\]]*)\]/,
  nolink: /^!?\[((?:\[[^\]]*\]|\\[\[\]]|[^\[\]])*)\]/,
  strong: /^__([\s\S]+?)__(?!_)|^\*\*([\s\S]+?)\*\*(?!\*)/,
  em: /^\b_([^\s](?:[^\\_]|\\[\s\S]|__)+?)_\b|^\*([^\s](?:\*\*|[\s\S])+?)\*(?!\*)/,
  em: /^\b_((?:[^_]|__)+?)_\b|^\*((?:\*\*|[\s\S])+?)\*(?!\*)/,
  em: /^_([^\s_](?:[^_]|__)+?[^\s_])_\b|^\*((?:\*\*|[^*])+?)\*(?!\*)/,
  code: /^(`+)\s*([\s\S]*?[^`]?)\s*\1(?!`)/,
  br: /^ {2,}\n(?!\s*$)/,
  del: noop,
  text: /^[\s\S]+?(?=[\\<!\[`*]|\b_| {2,}\n|$)/
};
inline._scheme = /[a-zA-Z][a-zA-Z0-9+.-]{1,31}/;
inline._email = /[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+(@)[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+(?![-_])/;
inline.autolink = edit(inline.autolink).replace('scheme', inline._scheme).replace('email', inline._email).getRegex();
inline._inside = /(?:\[[^\]^]*\]|\\[\[\]]|[^[\]]|\](?=[^[]*\]))*/;
inline._href = /\s*<?([\s\S]*?)>?(?:\s+['"]([\s\S]*?)['"])?\s*/;
inline.link = edit(inline.link).replace('inside', inline._inside).replace('href', inline._href).getRegex();
inline.reflink = edit(inline.reflink).replace('inside', inline._inside).getRegex();
inline.normal = merge({}, inline);
inline.pedantic = merge({}, inline.normal, {
  strong: /^__(?=\S)([\s\S]*?\S)__(?!_)|^\*\*(?=\S)([\s\S]*?\S)\*\*(?!\*)/,
  em: /^_(?=\S)([\s\S]*?\S)_(?!_)|^\*(?=\S)([\s\S]*?\S)\*(?!\*)/
});
inline.gfm = merge({}, inline.normal, {
  escape: edit(inline.escape).replace('])', '~|])').getRegex(),
  url: edit(/^((?:ftp|https?):\/\/|www\.)(?:[a-zA-Z0-9\-]+\.?)+[^\s<]*|^email/).replace('email', inline._email).getRegex(),
  _backpedal: /(?:[^?!.,:;*_~()&]+|\([^)]*\)|&(?![a-zA-Z0-9]+;$)|[?!.,:;*_~)]+(?!$))+/,
  del: /^~~(?=\S)([\s\S]*?\S)~~/,
  text: edit(inline.text).replace(']|', '~]|').replace('|', "|https?://|ftp://|www\\.|[a-zA-Z0-9.!#$%&'*+/=?^_`{\\|}~-]+@|").getRegex()
});
inline.breaks = merge({}, inline.gfm, {
  br: edit(inline.br).replace('{2,}', '*').getRegex(),
  text: edit(inline.gfm.text).replace('{2,}', '*').getRegex()
});
inline.extra = merge({}, inline.breaks, {
  footnote: /^\[\^(.+?)\]/
});

class InlineLexer {
  constructor(links, options = defaultOptions, footnotes = {}, usedFootnotes = []) {
    this.options = options;
    this.footnotes = footnotes;
    this.usedFootnotes = usedFootnotes;
    this.footnoteCounter = 0;
    this.links = links;
    this.renderer = this.options.renderer || new Renderer();
    this.renderer.options = this.options;
    if (!this.links) {
      throw new Error('Tokens array requires a `links` property.');
    }
    if (this.options.extra) {
      this.rules = inline.extra;
    } else if (this.options.gfm) {
      if (this.options.breaks) {
        this.rules = inline.breaks;
      } else {
        this.rules = inline.gfm;
      }
    } else if (this.options.pedantic) {
      this.rules = inline.pedantic;
    } else {
      this.rules = inline.normal;
    }
    let text = this.rules.text.toString();
    this.options.plugins.forEach(plugin => {
      if (plugin.inline && plugin.inline.text) {
        text = text.replace(']|', plugin.inline.text + ']|');
      }
    });
    this.rules.text = new RegExp(text.substring(1, text.length - 1));
    if (Array.isArray(this.options.disabledRules)) {
      this.options.disabledRules.forEach(ruleName => {
        if (this.rules[ruleName]) {
          this.rules[ruleName] = noop;
        }
      });
    }
  }
  static output(src, links, options, footnotes, usedFootnotes) {
    return new InlineLexer(links, options, footnotes, usedFootnotes).output(src);
  }
  output(src) {
    const plugins = this.options.plugins;
    let out = '';
    let link;
    let text;
    let href;
    let cap;
    let plugin;
    while (src) {
      if (cap = this.rules.escape.exec(src)) {
        src = src.substring(cap[0].length);
        out += cap[1];
        continue;
      }
      if (cap = this.rules.autolink.exec(src)) {
        src = src.substring(cap[0].length);
        if (cap[2] === '@') {
          text = escape(this.mangle(cap[1]));
          href = 'mailto:' + text;
        } else {
          text = escape(cap[1]);
          href = text;
        }
        out += this.renderer.link(href, null, text);
        continue;
      }
      if (!this.inLink && (cap = this.rules.url.exec(src))) {
        cap[0] = this.rules._backpedal.exec(cap[0])[0];
        src = src.substring(cap[0].length);
        if (cap[2] === '@') {
          text = escape(cap[0]);
          href = 'mailto:' + text;
        } else {
          text = escape(cap[0]);
          if (cap[1] === 'www.') {
            href = 'http://' + text;
          } else {
            href = text;
          }
        }
        out += this.renderer.link(href, null, text);
        continue;
      }
      if (cap = this.rules.tag.exec(src)) {
        if (!this.inLink && /^<a /i.test(cap[0])) {
          this.inLink = true;
        } else if (this.inLink && /^<\/a>/i.test(cap[0])) {
          this.inLink = false;
        }
        src = src.substring(cap[0].length);
        out += this.options.sanitize ? this.options.sanitizer ? this.options.sanitizer(cap[0]) : escape(cap[0]) : cap[0];
        continue;
      }
      if (cap = this.rules.footnote.exec(src)) {
        src = src.substring(cap[0].length);
        let m1 = escape(cap[1].toLowerCase());
        if (this.footnotes[m1]) {
          ++this.footnoteCounter;
          this.usedFootnotes.push(m1);
          out += `<sup id="cite-ref-${m1}" class="reference"><a href="#cite-${m1}">[${this.footnoteCounter}]</a></sup>`;
        } else {
          out += this.options.sanitize ? escape(cap[0]) : cap[0];
        }
        continue;
      }
      if (cap = this.rules.link.exec(src)) {
        src = src.substring(cap[0].length);
        this.inLink = true;
        out += this.outputLink(cap, {
          href: cap[2],
          title: cap[3]
        });
        this.inLink = false;
        continue;
      }
      if ((cap = this.rules.reflink.exec(src)) || (cap = this.rules.nolink.exec(src))) {
        src = src.substring(cap[0].length);
        link = (cap[2] || cap[1]).replace(/\s+/g, ' ');
        link = this.links[link.toLowerCase()];
        if (!link || !link.href) {
          out += cap[0].charAt(0);
          src = cap[0].substring(1) + src;
          continue;
        }
        this.inLink = true;
        out += this.outputLink(cap, link);
        this.inLink = false;
        continue;
      }
      if (cap = this.rules.strong.exec(src)) {
        src = src.substring(cap[0].length);
        out += this.renderer.strong(this.output(cap[2] || cap[1]));
        continue;
      }
      if (cap = this.rules.em.exec(src)) {
        src = src.substring(cap[0].length);
        out += this.renderer.em(this.output(cap[2] || cap[1]));
        continue;
      }
      if (cap = this.rules.code.exec(src)) {
        src = src.substring(cap[0].length);
        out += this.renderer.codespan(escape(cap[2].trim(), true));
        continue;
      }
      if (cap = this.rules.br.exec(src)) {
        src = src.substring(cap[0].length);
        out += this.renderer.br();
        continue;
      }
      if (cap = this.rules.del.exec(src)) {
        src = src.substring(cap[0].length);
        out += this.renderer.del(this.output(cap[1]));
        continue;
      }
      let isPluginMatch;
      for (let index = 0; index < plugins.length; index++) {
        plugin = plugins[index];
        if (plugin.inline) {
          if (cap = plugin.inline.rule.exec(src)) {
            src = src.substring(cap[0].length);
            out += plugin.render(cap);
            isPluginMatch = true;
            break;
          }
        }
      }
      if (isPluginMatch) continue;
      if (cap = this.rules.text.exec(src)) {
        src = src.substring(cap[0].length);
        out += this.renderer.text(escape(this.smartypants(cap[0])));
        continue;
      }
      if (src) {
        throw new Error('Infinite loop on byte: ' + src.charCodeAt(0));
      }
    }
    return out;
  }
  outputLink(cap, link) {
    const href = escape(link.href);
    const title = link.title ? escape(link.title) : null;
    return cap[0].charAt(0) === '!' ? this.renderer.image(href, title, escape(cap[1])) : this.renderer.link(href, title, this.output(cap[1]));
  }
  smartypants(text) {
    if (!this.options.smartypants) return text;
    return text
    .replace(/---/g, '\u2014')
    .replace(/--/g, '\u2013')
    .replace(/(^|[-\u2014/(\[{"\s])'/g, '$1\u2018')
    .replace(/'/g, '\u2019')
    .replace(/(^|[-\u2014/(\[{\u2018\s])"/g, '$1\u201c')
    .replace(/"/g, '\u201d')
    .replace(/\.{3}/g, '\u2026');
  }
  mangle(text) {
    if (!this.options.mangle) return text;
    let out = '';
    let i = 0;
    let ch;
    for (; i < text.length; i++) {
      ch = text.charCodeAt(i);
      if (Math.random() > 0.5) {
        ch = 'x' + ch.toString(16);
      }
      out += '&#' + ch + ';';
    }
    return out;
  }
}
InlineLexer.rules = inline;

class Parser {
  constructor(options = defaultOptions) {
    this.tokens = [];
    this.token = null;
    this.usedFootnotes = [];
    this.options = options;
    this.options.renderer = this.options.renderer || new Renderer();
    this.renderer = this.options.renderer;
    this.renderer.options = this.options;
  }
  static parse(src, options) {
    return new Parser(options).parse(src);
  }
  parse(src) {
    this.inline = new InlineLexer(src.links, this.options, src.footnotes, this.usedFootnotes);
    this.inlineText = new InlineLexer(src.links, merge({}, this.options, {
      renderer: new TextRenderer()
    }), src.footnotes, this.usedFootnotes);
    this.tokens = src.reverse();
    let out = '';
    while (this.next()) {
      out += this.tok();
    }
    if (this.usedFootnotes.length !== 0) {
      out += '<div class="references"><ol>';
      for (let i = 0; i < this.usedFootnotes.length; i++) {
        let id = this.usedFootnotes[i];
        let footnote = src.footnotes[id];
        out += `<li id="cite-${id}"><a href="#cite-ref-${id}" class="cite-backlink">^</a><span class="cite-text">${this.inline.output(footnote)}</span></li>`;
      }
      out += '</ol></div>';
    }
    this.renderer._headings = [];
    return out;
  }
  next() {
    this.token = this.tokens.pop();
    return this.token;
  }
  peek() {
    return this.tokens[this.tokens.length - 1] || 0;
  }
  parseText() {
    let body = this.token.text;
    while (this.peek().type === 'text') {
      body += `\n${this.next().text}`;
    }
    return this.inline.output(body);
  }
  tok() {
    const plugins = this.options.plugins;
    let plugin;
    switch (this.token.type) {
      case 'space':
        {
          return '';
        }
      case 'hr':
        {
          return this.renderer.hr();
        }
      case 'heading':
        {
          return this.renderer.heading(this.inline.output(this.token.text), this.token.depth, unescape(this.inlineText.output(this.token.text)));
        }
      case 'code':
        {
          return this.renderer.code(this.token.text, this.token.lang, this.token.escaped);
        }
      case 'table':
        {
          let header = '';
          let body = '';
          let i;
          let row;
          let cell;
          let j;
          cell = '';
          if (this.token.header.some(item => item.trim())) {
            for (i = 0; i < this.token.header.length; i++) {
              cell += this.renderer.tablecell(this.inline.output(this.token.header[i]), { header: true, align: this.token.align[i] });
            }
            header += this.renderer.tablerow(cell);
          }
          for (i = 0; i < this.token.cells.length; i++) {
            row = this.token.cells[i];
            cell = '';
            for (j = 0; j < row.length; j++) {
              cell += this.renderer.tablecell(this.inline.output(row[j]), {
                header: false,
                align: this.token.align[j]
              });
            }
            body += this.renderer.tablerow(cell);
          }
          return this.renderer.table(header, body);
        }
      case 'blockquote_start':
        {
          let body = '';
          while (this.next().type !== 'blockquote_end') {
            body += this.tok();
          }
          return this.renderer.blockquote(body);
        }
      case 'list_start':
        {
          let body = '';
          let taskList = false;
          const ordered = this.token.ordered;
          while (this.next().type !== 'list_end') {
            if (this.token.checked !== undefined) {
              taskList = true;
            }
            body += this.tok();
          }
          return this.renderer.list(body, ordered, taskList);
        }
      case 'list_item_start':
        {
          let body = '';
          const checked = this.token.checked;
          while (this.next().type !== 'list_item_end') {
            body += this.token.type === 'text' ? this.parseText() : this.tok();
          }
          return this.renderer.listitem(body, checked);
        }
      case 'loose_item_start':
        {
          let body = '';
          const checked = this.token.checked;
          while (this.next().type !== 'list_item_end') {
            body += this.tok();
          }
          return this.renderer.listitem(body, checked);
        }
      case 'html':
        {
          const html = !this.token.pre && !this.options.pedantic ? this.inline.output(this.token.text) : this.token.text;
          return this.renderer.html(html);
        }
      case 'paragraph':
        {
          return this.renderer.paragraph(this.inline.output(this.token.text));
        }
      case 'text':
        {
          return this.renderer.paragraph(this.parseText());
        }
      default:
        {
          for (let index = 0; index < plugins.length; index++) {
            plugin = plugins[index];
            if (plugin.block && this.token.type == plugin.block.type) {
              return plugin.render(this.token);
            }
          }
          throw new Error('Unknow type');
        }
    }
  }
}

const block = {
  newline: /^\n+/,
  code: /^( {4}[^\n]+\n*)+/,
  fences: noop,
  hr: /^ {0,3}((?:- *){3,}|(?:_ *){3,}|(?:\* *){3,})(?:\n+|$)/,
  heading: /^ *(#{1,6}) *([^\n]+?) *#* *(?:\n+|$)/,
  nptable: noop,
  blockquote: /^( {0,3}> ?(paragraph|[^\n]*)(?:\n|$))+/,
  list: /^( *)(bull) [\s\S]+?(?:hr|def|\n{2,}(?! )(?!\1bull )\n*|\s*$)/,
  html: /^ *(?:comment *(?:\n|\s*$)|closed *(?:\n{2,}|\s*$)|closing *(?:\n{2,}|\s*$))/,
  def: /^ {0,3}\[(label)\]: *\n? *<?([^\s>]+)>?(?:(?: +\n? *| *\n *)(title))? *(?:\n+|$)/,
  table: noop,
  lheading: /^([^\n]+)\n *(=|-){2,} *(?:\n+|$)/,
  paragraph: /^([^\n]+(?:\n?(?!hr|heading|lheading| {0,3}>|tag)[^\n]+)+)/,
  text: /^[^\n]+/
};
block._label = /(?:\\[\[\]]|[^\[\]])+/;
block._title = /(?:"(?:\\"|[^"]|"[^"\n]*")*"|'\n?(?:[^'\n]+\n?)*'|\([^()]*\))/;
block.def = edit(block.def).replace('label', block._label).replace('title', block._title).getRegex();
block.bullet = /(?:[*+-]|\d+\.)/;
block.item = /^( *)(bull) [^\n]*(?:\n(?!\1bull )[^\n]*)*/;
block.item = edit(block.item, 'gm').replace(/bull/g, block.bullet).getRegex();
block.list = edit(block.list).replace(/bull/g, block.bullet).replace('hr', '\\n+(?=\\1?(?:(?:- *){3,}|(?:_ *){3,}|(?:\\* *){3,})(?:\\n+|$))').replace('def', '\\n+(?=' + block.def.source + ')').getRegex();
block._tag = '(?!(?:' + 'a|em|strong|small|s|cite|q|dfn|abbr|data|time|code' + '|var|samp|kbd|sub|sup|i|b|u|mark|ruby|rt|rp|bdi|bdo' + '|span|br|wbr|ins|del|img)\\b)\\w+(?!:|[^\\w\\s@]*@)\\b';
block.html = edit(block.html).replace('comment', /<!--[\s\S]*?-->/).replace('closed', /<(tag)[\s\S]+?<\/\1>/).replace('closing', /<tag(?:"[^"]*"|'[^']*'|\s[^'"\/>]*)*?\/?>/).replace(/tag/g, block._tag).getRegex();
block.paragraph = edit(block.paragraph).replace('hr', block.hr).replace('heading', block.heading).replace('lheading', block.lheading).replace('blockquote', block.blockquote).replace('tag', '<' + block._tag).replace('def', block.def).getRegex();
block.blockquote = edit(block.blockquote).replace('paragraph', block.paragraph).getRegex();
block.normal = merge({}, block);
block.gfm = merge({}, block.normal, {
  fences: /^ *(`{3,}|~{3,})[ \.]*(\S+)? *\n([\s\S]*?)\n? *\1 *(?:\n+|$)/,
  paragraph: /^/,
  heading: /^ *(#{1,6}) +([^\n]+?) *#* *(?:\n+|$)/,
  checkbox: /^\[([ x])\] +/
});
block.gfm.paragraph = edit(block.paragraph).replace('(?!', '(?!' + block.gfm.fences.source.replace('\\1', '\\2') + '|' + block.list.source.replace('\\1', '\\3') + '|').getRegex();
block.tables = merge({}, block.gfm, {
  nptable: /^(?: *(\S.*\|.*)\n)? *([-:]+ *\|[-| :]*)\n((?:.*\|.*(?:\n|$))*)\n*/,
  table: /^(?: *\|(.+)\n)? *\|( *[-:]+[-| :]*)\n((?: *\|.*(?:\n|$))*)\n*/
});
block.extra = merge({}, block.tables, {
  footnote: /^ *\[\^([^\]]+)\]: *(.*)(?:\n+|$)/
});

class Lexer {
  constructor(options = defaultOptions) {
    this.tokens = [];
    this.tokens.links = {};
    this.tokens.footnotes = {};
    this.tokens.ext = {};
    this.options = options;
    if (this.options.extra) {
      this.rules = block.extra;
    } else if (this.options.gfm) {
      if (this.options.tables) {
        this.rules = block.tables;
      } else {
        this.rules = block.gfm;
      }
    } else {
      this.rules = block.normal;
    }
    if (Array.isArray(this.options.disabledRules)) {
      this.options.disabledRules.forEach(ruleName => {
        if (this.rules[ruleName]) {
          this.rules[ruleName] = noop;
        }
      });
    }
  }
  static lex(src, options) {
    return new Lexer(options).lex(src);
  }
  lex(src) {
    src = src.replace(/\r\n|\r/g, '\n').replace(/\t/g, '    ').replace(/\u00a0/g, ' ').replace(/\u2424/g, '\n');
    return this.token(src, true);
  }
  token(src, top) {
    src = src.replace(/^ +$/gm, '');
    const plugins = this.options.plugins;
    let next;
    let loose;
    let cap;
    let bull;
    let b;
    let item;
    let space;
    let i;
    let tag;
    let l;
    let checked;
    let plugin;
    while (src) {
      if (cap = this.rules.newline.exec(src)) {
        src = src.substring(cap[0].length);
        if (cap[0].length > 1) {
          this.tokens.push({
            type: 'space'
          });
        }
      }
      if (cap = this.rules.code.exec(src)) {
        src = src.substring(cap[0].length);
        cap = cap[0].replace(/^ {4}/gm, '');
        this.tokens.push({
          type: 'code',
          text: this.options.pedantic ? cap : cap.replace(/\n+$/, '')
        });
        continue;
      }
      if (cap = this.rules.fences.exec(src)) {
        src = src.substring(cap[0].length);
        this.tokens.push({
          type: 'code',
          lang: cap[2],
          text: cap[3] || ''
        });
        continue;
      }
      if (cap = this.rules.heading.exec(src)) {
        src = src.substring(cap[0].length);
        this.tokens.push({
          type: 'heading',
          depth: cap[1].length,
          text: cap[2]
        });
        continue;
      }
      if (top && (cap = this.rules.nptable.exec(src))) {
        src = src.substring(cap[0].length);
        item = {
          type: 'table',
          header: cap[1] && cap[1].replace(/^ *| *\| *$/g, '').split(/ *\| */),
          align: cap[2].replace(/^ *|\| *$/g, '').split(/ *\| */),
          cells: cap[3].replace(/\n$/, '').split('\n')
        };
        for (i = 0; i < item.align.length; i++) {
          if (/^ *-+: *$/.test(item.align[i])) {
            item.align[i] = 'right';
          } else if (/^ *:-+: *$/.test(item.align[i])) {
            item.align[i] = 'center';
          } else if (/^ *:-+ *$/.test(item.align[i])) {
            item.align[i] = 'left';
          } else {
            item.align[i] = null;
          }
        }
        for (i = 0; i < item.cells.length; i++) {
          item.cells[i] = item.cells[i].split(/ *[^`\\]\| */);
        }
        this.tokens.push(item);
        continue;
      }
      if (cap = this.rules.hr.exec(src)) {
        src = src.substring(cap[0].length);
        this.tokens.push({
          type: 'hr'
        });
        continue;
      }
      if (cap = this.rules.blockquote.exec(src)) {
        src = src.substring(cap[0].length);
        this.tokens.push({
          type: 'blockquote_start'
        });
        cap = cap[0].replace(/^ *> ?/gm, '');
        this.token(cap, top);
        this.tokens.push({
          type: 'blockquote_end'
        });
        continue;
      }
      if (cap = this.rules.list.exec(src)) {
        src = src.substring(cap[0].length);
        bull = cap[2];
        this.tokens.push({
          type: 'list_start',
          ordered: bull.length > 1
        });
        cap = cap[0].match(this.rules.item);
        next = false;
        l = cap.length;
        i = 0;
        for (; i < l; i++) {
          item = cap[i];
          space = item.length;
          item = item.replace(/^ *([*+-]|\d+\.) +/, '');
          if (this.options.gfm && this.options.taskLists) {
            checked = this.rules.checkbox.exec(item);
            if (checked) {
              checked = checked[1] === 'x';
              item = item.replace(this.rules.checkbox, '');
            } else {
              checked = undefined;
            }
          }
          if (item.indexOf('\n ') !== -1) {
            space -= item.length;
            item = this.options.pedantic ? item.replace(/^ {1,4}/gm, '') : item.replace(new RegExp('^ {1,' + space + '}', 'gm'), '');
          }
          if (this.options.smartLists && i !== l - 1) {
            b = this.rules.bullet.exec(cap[i + 1])[0];
            if (bull !== b && !(bull.length > 1 && b.length > 1)) {
              src = cap.slice(i + 1).join('\n') + src;
              i = l - 1;
            }
          }
          loose = next || /\n\n(?!\s*$)/.test(item);
          if (i !== l - 1) {
            next = item.charAt(item.length - 1) === '\n';
            if (!loose) loose = next;
          }
          this.tokens.push({
            checked,
            type: loose ? 'loose_item_start' : 'list_item_start'
          });
          this.token(item, false);
          this.tokens.push({
            type: 'list_item_end'
          });
        }
        this.tokens.push({
          type: 'list_end'
        });
        continue;
      }
      if (cap = this.rules.html.exec(src)) {
        src = src.substring(cap[0].length);
        this.tokens.push({
          type: this.options.sanitize ? 'paragraph' : 'html',
          pre: !this.options.sanitizer && (cap[1] === 'pre' || cap[1] === 'script' || cap[1] === 'style'),
          text: cap[0]
        });
        continue;
      }
      if (top && (cap = this.rules.def.exec(src))) {
        src = src.substring(cap[0].length);
        if (cap[3]) cap[3] = cap[3].substring(1, cap[3].length - 1);
        tag = cap[1].toLowerCase();
        if (!this.tokens.links[tag]) {
          this.tokens.links[tag] = {
            href: cap[2],
            title: cap[3]
          };
        }
        continue;
      }
      if (top && (cap = this.rules.table.exec(src))) {
        src = src.substring(cap[0].length);
        item = {
          type: 'table',
          header: cap[1] && cap[1].replace(/^ *| *\| *$/g, '').split(/ *\| */),
          align: cap[2].replace(/^ *|\| *$/g, '').split(/ *\| */),
          cells: cap[3].replace(/\n$/, '').split('\n')
        };
        for (i = 0; i < item.align.length; i++) {
          if (/^ *-+: *$/.test(item.align[i])) {
            item.align[i] = 'right';
          } else if (/^ *:-+: *$/.test(item.align[i])) {
            item.align[i] = 'center';
          } else if (/^ *:-+ *$/.test(item.align[i])) {
            item.align[i] = 'left';
          } else {
            item.align[i] = null;
          }
        }
        for (i = 0; i < item.cells.length; i++) {
          if (!item.cells[i].match(/\|$/)) {
            item.cells[i] = item.cells[i] + '|';
          }
          item.cells[i] = item.cells[i].replace(/^ *\| *| *\| *$/g, '')
          .split(/ *[^`\\]\| */);
        }
        this.tokens.push(item);
        continue;
      }
      if (cap = this.rules.footnote.exec(src)) {
        src = src.substring(cap[0].length);
        this.tokens.footnotes[escape(cap[1].toLowerCase())] = cap[2];
        continue;
      }
      if (cap = this.rules.lheading.exec(src)) {
        src = src.substring(cap[0].length);
        this.tokens.push({
          type: 'heading',
          depth: cap[2] === '=' ? 1 : 2,
          text: cap[1]
        });
        continue;
      }
      let isPluginMatch;
      for (let index = 0; index < plugins.length; index++) {
        plugin = plugins[index];
        if (plugin.block) {
          if (cap = plugin.block.rule.exec(src)) {
            if (cap.index === 0) {
              src = src.substring(cap[0].length);
              const token = plugin.block.tokenize(cap);
              this.tokens.push(token);
              isPluginMatch = true;
              break;
            }
          }
        }
      }
      if (isPluginMatch) continue;
      if (top && (cap = this.rules.paragraph.exec(src))) {
        src = src.substring(cap[0].length);
        this.tokens.push({
          type: 'paragraph',
          text: cap[1].charAt(cap[1].length - 1) === '\n' ? cap[1].slice(0, -1) : cap[1]
        });
        continue;
      }
      if (cap = this.rules.text.exec(src)) {
        src = src.substring(cap[0].length);
        this.tokens.push({
          type: 'text',
          text: cap[0]
        });
        continue;
      }
      if (src) {
        throw new Error('Infinite loop on byte: ' + src.charCodeAt(0));
      }
    }
    return this.tokens;
  }
}
Lexer.rules = block;

function marked(src, opt) {
  if (typeof src === 'undefined' || src === null) throw new Error('marked(): input parameter is undefined or null');
  if (typeof src !== 'string') throw new Error('marked(): input parameter is of type ' + Object.prototype.toString.call(src) + ', string expected');
  try {
    if (opt) opt = merge({}, defaultOptions, opt);
    return Parser.parse(Lexer.lex(src, opt), opt);
  } catch (err) {
    if ((opt || defaultOptions).silent) {
      return '<p>An error occurred:</p><pre>' + escape(String(err.message), true) + '</pre>';
    }
    throw err;
  }
}
marked.Renderer = Renderer;
marked.TextRenderer = TextRenderer;
marked.Parser = Parser;
marked.Lexer = Lexer;
marked.InlineLexer = InlineLexer;
marked.defaults = defaultOptions;
marked.utils = utils;

export default marked;
//# sourceMappingURL=marked.es.js.map
