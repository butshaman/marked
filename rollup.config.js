import babel from 'rollup-plugin-babel'
import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import cleanup from 'rollup-plugin-cleanup'
import progress from 'rollup-plugin-progress'
import filesize from 'rollup-plugin-filesize'
import pkg from './package.json'

export default {
  input: 'src/index.js',
  plugins: [
    babel({
      exclude: 'node_modules/**'
    }),
    resolve({
      modulesOnly: true,
      jsnext: true,
      main: true,
      browser: true
    }),
    commonjs(),
    cleanup({
      comments: 'none'
    }),
    progress(),
    filesize()
  ],
  external: Object.keys(pkg.dependencies),
  strict: false,
  sourcemap: true,
  output: [
    {
      format: 'es',
      file: pkg.module
    },
    {
      format: 'cjs',
      file: pkg.main
    }
  ]
}
